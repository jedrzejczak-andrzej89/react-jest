import React from 'react';
import { shallow } from 'enzyme';

import Calculator from 'components/Calculator';
import CalculatorPage from '../index';

describe('<CalculatorPage />', () => {
  it('should render its heading', () => {
    const renderedComponent = shallow(<CalculatorPage />);
    expect(renderedComponent.contains(<Calculator />)).toBe(true);
  });
});
