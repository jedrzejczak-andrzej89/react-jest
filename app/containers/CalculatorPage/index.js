/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import Calculator from 'components/Calculator';

const CalculatorPage = () => (
  <div>
    <Helmet>
      <title>Calculator Page</title>
      <meta
        name="description"
        content="Feature page of React.js Boilerplate application"
      />
    </Helmet>
    <Calculator />
  </div>
);

export default CalculatorPage;
