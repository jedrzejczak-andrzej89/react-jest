import { add } from './../index';

describe('add()', () => {
  it('adds two numbers', () => {
    expect(add(2, 3)).toEqual(5);
  });

  it('adds two string-numbers', () => {
    expect(add('12', '3')).toEqual(15);
  });

  it("doesn't add the third number", () => {
    expect(add(2, 3, 5)).toEqual(add(2, 3));
  });
});
