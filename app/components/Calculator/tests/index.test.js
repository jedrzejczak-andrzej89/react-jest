import React from 'react';
import { shallow } from 'enzyme';

import Calculator from '../index';

describe('<Calculator />', () => {
  it('should render a div by id calculator', () => {
    const renderedComponent = shallow(<Calculator />);
    expect(renderedComponent.prop('id')).toEqual('calculator');
  });

  it('should render two inputs and result', () => {
    const renderedComponent = shallow(<Calculator />);

    const input = renderedComponent.find('#calculator--firstNumber');
    expect(input.length).toBeGreaterThan(0);

    const secondNumberInput = renderedComponent.find(
      '#calculator--secondNumber',
    );
    expect(secondNumberInput.length).toBeGreaterThan(0);

    const result = renderedComponent.find('#calculator--result');
    expect(result.length).toBeGreaterThan(0);
  });

  it('should render two inputs with init values equal 0', () => {
    const renderedComponent = shallow(<Calculator />);

    const input = renderedComponent.find('#calculator--firstNumber');
    expect(input.prop('value')).toEqual(0);

    const secondNumberInput = renderedComponent.find(
      '#calculator--secondNumber',
    );
    expect(secondNumberInput.prop('value')).toEqual(0);
  });

  it('initial component state', () => {
    const renderedComponent = shallow(<Calculator />);
    expect(renderedComponent.state().firstNumber).toEqual(0);
    expect(renderedComponent.state().secondNumber).toEqual(0);
  });

  it('handleChangeFirstNumber update state', () => {
    const renderedComponent = shallow(<Calculator />);

    const newFirstNumber = 5;

    const event = {
      preventDefault() {},
      target: { value: null },
    };
    const input = renderedComponent.find('#calculator--firstNumber');
    event.target.value = newFirstNumber;

    input.simulate('change', event);

    expect(renderedComponent.state().firstNumber).toEqual(newFirstNumber);
  });

  it('handleChangeSecondNumber update state', () => {
    const renderedComponent = shallow(<Calculator />);

    const newSecondNumber = 15;

    const event = {
      preventDefault() {},
      target: { value: null },
    };
    const input = renderedComponent.find('#calculator--secondNumber');
    event.target.value = newSecondNumber;

    input.simulate('change', event);

    expect(renderedComponent.state().secondNumber).toEqual(newSecondNumber);
  });
});
