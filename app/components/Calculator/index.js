import React from 'react';

import { add } from 'utils/calculator/index';

class Calculator extends React.Component {
  constructor() {
    super();
    this.state = {
      firstNumber: 0,
      secondNumber: 0,
    };
    this.handleChangeFirstNumber = this.handleChangeFirstNumber.bind(this);
    this.handleChangeSecondNumber = this.handleChangeSecondNumber.bind(this);
  }
  handleChangeFirstNumber(event) {
    this.setState({ firstNumber: event.target.value });
  }
  handleChangeSecondNumber(event) {
    this.setState({ secondNumber: event.target.value });
  }
  render() {
    return (
      <div id="calculator">
        <input
          type="number"
          id="calculator--firstNumber"
          value={this.state.firstNumber}
          onChange={this.handleChangeFirstNumber}
        />
        +
        <input
          type="number"
          id="calculator--secondNumber"
          value={this.state.secondNumber}
          onChange={this.handleChangeSecondNumber}
        />
        =
        <span id="calculator--result">
          {add(this.state.firstNumber, this.state.secondNumber)}
        </span>
      </div>
    );
  }
}

export default Calculator;
